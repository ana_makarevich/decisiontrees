﻿using System;

using System.Net;

using System.Data;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace DecisionTrees
{
	public class MainClass
	{
        public static void Main()
        {
            /***************************************
             *             IRIS DATASET            *
             * *************************************/
            DecTreeUtils irisUtils = new DecTreeUtils(3);
            // Read the dataset from file
            string[] lines = System.IO.File.ReadAllLines(@"iris.txt");

            // Create a datatable
            DataTable iris = new DataTable("iris");

            // Set column names
            List<string> attributes = new List<string>();
            iris.Columns.Add("Sepal.Length", typeof(double));
            attributes.Add("Sepal.Length");
            iris.Columns.Add("Sepal.Width", typeof(double));
            attributes.Add("Sepal.Width");
            iris.Columns.Add("Petal.Length", typeof(double));
            attributes.Add("Petal.Length");
            iris.Columns.Add("Petal.Width", typeof(double));
            attributes.Add("Petal.Width");
            iris.Columns.Add("Species", typeof(string));

            // Load data into a datatable
            foreach (string line in lines)
            {
                // Tokenized
                string[] values = line.Split(new Char[] { ',' });
                // Check if the row is valid
                if (values.Length == 5)
                {
                    iris.Rows.Add(
                        Convert.ToDouble(values[0]),
                        Convert.ToDouble(values[1]),
                        Convert.ToDouble(values[2]),
                        Convert.ToDouble(values[3]),
                        values[4]);
                }
            }

            /**
             * Simple discretization
             * */

            DataTable dt1 = iris.Copy();

            dt1 = irisUtils.simpleDiscretize(dt1, "Sepal.Length");
            dt1 = irisUtils.simpleDiscretize(dt1, "Sepal.Width");
            dt1 = irisUtils.simpleDiscretize(dt1, "Petal.Length");
            dt1 = irisUtils.simpleDiscretize(dt1, "Petal.Width");

            Console.WriteLine("Simple Discretization");
            //DecTreeUtils.printDT(dt1);

            // Split data into train and test
            Dictionary<string, DataTable> sd_split = irisUtils.getSplit(dt1, 0.8, "Species");
            // Initialize a tree
            DecisionTree iris_tree_train1 = new DecisionTree(sd_split["train"],3);
            // Build a tree
            Node iris_root_train1 = iris_tree_train1.ID3("Species", sd_split["train"], attributes, "root");
            // Show the tree
            iris_tree_train1.PrintTree(iris_root_train1);

            // Evaluate the results
            string answer;
            double correct = 0;
            double total = sd_split["test"].Rows.Count;
            foreach (DataRow row in sd_split["test"].Rows)
            {
                //DecTreeUtils.printRowDT(row);
                answer = iris_tree_train1.classify(iris_root_train1, row);
                //Console.WriteLine("Test result: " + answer);
                if (answer.Equals(row["Species"])) {
                    correct++;
                }
            }
            Console.WriteLine("Accuracy: " + correct / total);


            /**
            * Entorpy discretization
            * */
            DataTable dt2 = iris.Copy();
            dt2 = irisUtils.entropyDiscretize(dt2, "Sepal.Length", "Species");
            dt2 = irisUtils.entropyDiscretize(dt2, "Sepal.Width", "Species");
            dt2 = irisUtils.entropyDiscretize(dt2, "Petal.Length", "Species");
            dt2 = irisUtils.entropyDiscretize(dt2, "Petal.Width", "Species");

            Console.WriteLine("Entropy  Discretization");
            //DecTreeUtils.printDT(dt2);


            // Total entropy
            //Console.WriteLine("Iris dataset entropy: " + irisUtils.calculateEntropy(iris, "Species"));
            //string best = irisUtils.getBestAttribute(dt2, attributes, "Species");
            //Console.WriteLine("Best attribute: " + best);


            // Now the fun begins
            //DecisionTree tree = new DecisionTree(dt2);
            //Node root = tree.ID3("Species", dt2, attributes, "root");

            // Show me the mone... the tree, baby!
            //tree.PrintTree(root);
            Dictionary<string, DataTable> ed_split = irisUtils.getSplit(dt2, 0.7, "Species");
            //Console.WriteLine("Train: " + ed_split["train"].Rows.Count);
            //Console.WriteLine("Test: " + ed_split["test"].Rows.Count);
            DecisionTree iris_tree_train2 = new DecisionTree(ed_split["train"],3);
            Node iris_root_train2 = iris_tree_train2.ID3("Species", ed_split["train"], attributes, "root");
            iris_tree_train2.PrintTree(iris_root_train2);
            correct = 0;
            total = ed_split["test"].Rows.Count;
            foreach (DataRow row in ed_split["test"].Rows)
            {
                //DecTreeUtils.printRowDT(row);
                answer = iris_tree_train2.classify(iris_root_train2, row);
                //Console.WriteLine("Test result: " + answer);
                if (answer.Equals(row["Species"]))
                {
                    correct++;
                }
            }
            Console.WriteLine("Accuracy: " + correct / total);

            /***************************************
             *             ZOO DATASET             *
             * *************************************/
            // Read the dataset from file
            DecTreeUtils zooUtils = new DecTreeUtils(7);
            string[] lines2 = System.IO.File.ReadAllLines(@"zoo.txt");
            // Create a datatable
            DataTable zoo = new DataTable("zoo");

            // Set column names
            string[] zoo_attributes1 = {"hair", "feathers", "eggs",
                "milk", "airborne", "aquatic","predator","toothed","backbone",
                "breathes","venomous","fins","legs","tail","domestic","catsize"};
            List<string> zoo_attributes = zoo_attributes1.ToList();
            foreach (string attr in zoo_attributes)
            {
                zoo.Columns.Add(attr, typeof(string));
            }
            zoo.Columns.Add("type", typeof(string));
            foreach (string line in lines2)
            {
                // Tokenized
                string[] values = line.Split(new Char[] { ',' });
                // Check if the row is valid
                if (values.Length == 18)
                {
                    zoo.Rows.Add( values[1], values[2],
                                 values[3], values[4], values[5],
                                 values[6], values[7], values[8],
                                 values[9], values[10], values[11],
                                 values[12], values[13], values[14],
                                 values[15], values[16], values[17]);
                }
            } // end of adding rows

            //DecTreeUtils.printDT(zoo);
            /*
            Console.WriteLine("Zoo dataset entropy: " + zooUtils.calculateEntropy(zoo, "type"));
            best = zooUtils.getBestAttribute(zoo, zoo_attributes, "type");
            Console.WriteLine("Best attribute: " + best);

            DecisionTree tree2 = new DecisionTree(zoo);
            Node root2 = tree2.ID3("type", zoo, zoo_attributes, "root");
            tree2.PrintTree(root2);
            */
            Dictionary<string, DataTable> zoo_split = zooUtils.getSplit(zoo, 0.7, "type");
            Console.WriteLine("Zoo train: " + zoo_split["train"].Rows.Count);
            Console.WriteLine("Zoo test: " + zoo_split["test"].Rows.Count);
            DecisionTree tree3 = new DecisionTree(zoo_split["train"],7);
            Node root3 = tree3.ID3("type", zoo_split["train"], zoo_attributes, "root");
            tree3.PrintTree(root3);
            correct = 0;
            total = zoo_split["test"].Rows.Count;
            foreach (DataRow row in zoo_split["test"].Rows)
            {
                //DecTreeUtils.printRowDT(row);
                answer = tree3.classify(root3, row);
                //Console.WriteLine("Test result: " + answer);
                if (answer.Equals(row["type"]))
                {
                    correct++;
                }
            }
            Console.WriteLine("Accuracy: " + correct / total);
		}
	}
}


