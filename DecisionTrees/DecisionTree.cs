﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DecisionTrees
{
    public class DecisionTree
    {
        private DataTable original_dataset;
        //public int count;
        private string ws = " ";
        private int n_classes;

        public DecisionTree(DataTable dt,int n_classes)
        {
            //count = 0;
            this.original_dataset = dt;
            this.n_classes = n_classes;
        }

        /// <summary>
        /// Classic ID3 algorithm based on recursion
        /// </summary>
        /// <param name="class_column">Class column.</param>
        /// <param name="dt">Dt.</param>
        /// <param name="attributes">Attributes.</param>
        /// <param name="branch_name">Branch name.</param>
        public Node ID3(string class_column, DataTable dt, List<string> attributes, string branch_name)
        {


            // Create root node for the tree
            Node root = new Node();
            // if all belong to the same class
            // Find all possible values in the target column
            // and check if all the examples belong to one class
            var labels = dt.AsEnumerable().Select(r => r.Field<string>(class_column)).ToList().Distinct();
            if (labels.Count() == 1)
            {
                root.label = labels.First();
                root.value = branch_name;
                return root;
            }
            // if attributes list is empty, return the most popular class
            if (attributes.Count() == 0)
            {
                root.label = getMostFrequentLabel(dt, class_column, labels);
                root.value = branch_name;
                return root;
            }
            // find the best attribute 
            string best_attribute = (new DecTreeUtils(n_classes)).getBestAttribute(dt, attributes, class_column);
            // set the decision attribute for the current node
            root.attribute = best_attribute;
            root.value = branch_name;

            // get all possible values of this attribute FOR THE WHOLE dataset:
            var values = original_dataset.AsEnumerable().Select(r => r.Field<string>(best_attribute)).ToList().Distinct();

            foreach (string val in values)
            {
                // select the subset of all examples with this value
                DataRow[] selected_rows = dt.Select(best_attribute + " = '" + val + "'");


                // if no observations with the given attribute value
                if (selected_rows.Count() == 0)
                {
                    Node node = new Node();
                    node.label = getMostFrequentLabel(dt, class_column, labels);
                    node.value = best_attribute + " = " +  val;
                    root.children.Add(node);


                }
                else {

                    Node node = ID3(class_column, 
                                    selected_rows.CopyToDataTable(), 
                                    attributes.Except(new List<string>() { best_attribute }).ToList(),
                                    best_attribute + " = " +val);
                    root.children.Add(node);
                    
                }

            }

            return root;
        }

        /// <summary>
        /// Gets the most frequent label in the given dataset
        /// </summary>
        /// <returns>The most frequent label.</returns>
        /// <param name="dt">Datatable to evaluate.</param>
        /// <param name="class_column">Name of the column with the target.</param>
        /// <param name="labels">All possible values of the target function.</param>
        private string getMostFrequentLabel(DataTable dt, string class_column, IEnumerable<string> labels)
        {
            int mx = 0;
            string label = labels.First();
            foreach (var l in labels)
            {
                DataRow[] result = dt.Select(class_column + " = '" + l +"'");
                if (result.Length >= mx)
                {
                    mx = result.Length;
                    label = l;
                }
            }

            return label;
        }


        /// <summary>
        /// This is a super cool tree printing function.
        /// Waht cool about it is that it prints a tree into console
        /// And still it's pretty easy to understand what's goint on.
        /// Surprise, surprise.
        /// </summary>
        /// <param name="root">Root.</param>
        public void PrintTree(Node root)
        {
            
            Console.WriteLine(ws + root.value);
            if (root.attribute != null)
            {
                ws = ws + "  ";
                Console.WriteLine(ws + "SPLITTED BY: " + root.attribute);
                foreach (Node node in root.children)
                {
                    PrintTree(node);
                    ws = ws.Substring(2);
                }
                
            }

            if (root.isLeaf())
            {
                Console.WriteLine(ws + " --->  " + root.label);
                ws = ws + "  ";

            }
           
        }

        public string classify(Node root, DataRow example)
        {
            if (root.isLeaf())
            {
                return root.label;
            }

            if (root.attribute != null)
            {
                string value = root.attribute + " = " + (string)example[root.attribute];
                //Console.WriteLine("Example value: " + value);
                foreach (Node node in root.children)
                {
                    if (value.Equals(node.value))
                    {
                        //Console.WriteLine("Node value: " + node.value);
                        return classify(node, example);
                    }
                }

            }

            return "Can't classify";

        }
    

    }
}

