﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DecisionTrees
{
    public class DecTreeUtils
    {
        private int n_classes;
        public DecTreeUtils(int n_classes)
        {
            this.n_classes = n_classes;
        }

        /// <summary>
        /// 
        /// Performs simple attribute discretization that doesn't take
        /// into account the value of the target function
        /// </summary>
        /// <returns>DataTable with the discretized column with col_name column</returns>
        /// <param name="dt">original data table</param>
        /// <param name="col_name">the name of the column to be discretized</param>
        public DataTable simpleDiscretize(DataTable dt, string col_name)
        {
            
            var values =  dt.AsEnumerable().Select(r => r.Field<double>(col_name)).ToList();
            List<double> thresholds = new List<double>();
            double step = (values.Max() - values.Min()) / 3;

            double th1 = values.Min() + step;
            thresholds.Add(th1);
            double th2 = th1 + step;
            thresholds.Add(th2);
            //double th3 = th2 + step;
            //thresholds.Add(th3);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                
                dt.Rows[i][col_name] = getDiscrete((double)dt.Rows[i][col_name],thresholds);
            }

            //Change the datatype of the column to string
            DataTable dtCloned = dt.Clone();
            dtCloned.Columns[col_name].DataType = typeof(string);

            foreach (DataRow row in dt.Rows)
            {
              dtCloned.ImportRow(row);
            }
            return dtCloned;
        }

        /// <summary>
        /// Returns the discrete value for the class.
        /// Discretisizes by comparing to each threshold value. 
        /// Once it finds the upper bound, the interval id is returned.
        /// </summary>
        /// <returns>Discretized value</returns>
        /// <param name="x">Continious value</param>
        /// <param name="thresholds">Thresholds list</param>
        private int getDiscrete(double x,List<double> thresholds)
        {
            for (int i = 0; i < thresholds.Count; i++)
            {
                if (x < thresholds[i])
                {
                    return i + 1;
                }
            }
            return thresholds.Count+1;
        }



        /// <summary>
        /// Perfrom discretization with respcet to the value of the 
        /// target attribute. Calculates entropy.
        /// </summary>
        /// <returns>DataTable with the discretized column with col_name column</returns>
        /// <param name="dt">original data table</param>
        /// <param name="col_name">the name of the column to be discretized</param>
        /// <param name="class_column">the name of the column with the target attribute</param>
        public DataTable entropyDiscretize(DataTable dt, string col_name, string class_column)
        {
            // sort rows with respect to the col_name column
            DataRow[] dataRows = dt.Select().OrderBy(u => u[col_name]).ToArray();
            dt = dataRows.CopyToDataTable();

            // initialize threshold candidates
            List<double> thresholds = new List<double>();
            // initialize the current class
            string cur_class = (string)dt.Rows[1][class_column];
            for (int i = 1; i < dt.Rows.Count; i++)
            {
                if ((string)dt.Rows[i][class_column] != cur_class)
                {
                    cur_class = (string)dt.Rows[i][class_column];
                    double new_threshold = ((double)dt.Rows[i][col_name] + (double)dt.Rows[i - 1][col_name]) / 2;

                    thresholds.Add(new_threshold);
                }
            }
            thresholds = thresholds.Distinct().ToList();
            List<double> best_thresholds = getBestThresholds(dt, class_column, thresholds, col_name);
            Console.WriteLine(col_name);
            best_thresholds.ForEach(Console.WriteLine);
            DataTable dt2 = discretizeDT(dt, col_name, best_thresholds);

            return dt2;
        }

        /// <summary>
        /// Calculates entropy for the given dataset,
        /// where class_column represents the target value column
        /// </summary>
        /// <returns>The entropy.</returns>
        /// <param name="dt">original dataset</param>
        /// <param name="class_column">target value column name</param>
        public double calculateEntropy(DataTable dt, string class_column)
        {
            // get all possible classes

            var labels = dt.AsEnumerable().Select(r => r.Field<string>(class_column)).ToList().Distinct();

            double n_rows = dt.Rows.Count;
            double entropy = 0;

            foreach (string label in labels)
            {
                DataRow[] result = dt.Select(class_column + " = '" + label + "'");
                double p = result.Count() / n_rows;
                // TODO: Check the correct base for the logarithm
                entropy -=  p * Math.Log(p, 2);

            }
            return entropy;

        }

        /// <summary>
        /// Calculates the information gain with the given thresholds.
        /// </summary>
        /// <returns>The information gain.</returns>
        /// <param name="dt">Dt.</param>
        /// <param name="class_column">Name of the column with the target value</param>
        /// <param name="thresholds">Best thresholds.</param>
        /// <param name="entropy">Entropy.</param>
        /// <param name="col_name">Name of the column to be discretized.</param>
        public double calculateInformationGain2(DataTable dt,
                                                       string class_column,
                                                       double[] thresholds,
                                                       double entropy,
                                                       string col_name)
        {
            //sort the thresholds
            Array.Sort(thresholds);
            //total number of rows
            double n_rows = dt.Rows.Count;
            // initialize 
            double sum_ent = 0;
            DataRow[] dr1 = dt.Select(col_name + "<=" + thresholds[0]);
            if (dr1.Count() > 0)
            {
                DataTable dt1 = dr1.CopyToDataTable();
                double ent1 = calculateEntropy(dt1, class_column);
                sum_ent += (dr1.Count() / n_rows) * ent1;
            }
            for (int i = 1; i < thresholds.Length; i++)
            {
                DataRow[] cur_dr = dt.Select(col_name + ">" + 
                                             thresholds[i - 1] + " AND " + 
                                             col_name + "<=" + thresholds[i]);
                if (cur_dr.Count() > 0)
                {
                    DataTable cur_dt = cur_dr.CopyToDataTable();
                    double ent = calculateEntropy(cur_dt, class_column);
                    sum_ent += (cur_dr.Count() / n_rows) * ent;
                }

            }
            // last threshold
            DataRow[] drn = dt.Select(col_name + ">" + thresholds[thresholds.Count()-1]);
            DataTable dtn = drn.CopyToDataTable();
            double entn = calculateEntropy(dtn, class_column);
            sum_ent += (drn.Count() / n_rows) * entn;

            return (entropy - sum_ent);
        }
        /// <summary>
        /// Finds 3 best thresholds by finding one best threshold at a time and then sequentially 
        /// adding a new threshold and calculating gain on each new combination of the best and the new. 
        /// </summary>
        /// <returns>The best thresholds.</returns>
        /// <param name="dt">Original dataset</param>
        /// <param name="class_column">Name of the column to be discretized</param>
        /// <param name="thresholds">Candidate thresholds.</param>
        /// <param name="col_name">The name of the column to discretize</param>
        public List<double> getBestThresholds(DataTable dt, 
                                                     string class_column, 
                                                     List<double> thresholds, 
                                                     string col_name)
        {
            double entropy = calculateEntropy(dt, class_column);
            // Now we need to calculate and compare the information gain
            //Console.WriteLine("Thresholds: " + thresholds.Count());

            // Find the best threshold
            List<double> best_thresholds = new List<double>();
            // find 2 best thresholds
            for (int i = 0; i < 3; i++)
            {
                double max_gain = 0;
                double best_threshold = 0;
                double[] cur_thresholds = new double[i + 1];
                for (int j = 0; j < i; j++)
                {
                    cur_thresholds[j] = best_thresholds[j];
                }

                foreach (double threshold in thresholds)
                {
                    cur_thresholds[i] = threshold;
                    double gain = calculateInformationGain2(dt, 
                                                            class_column, 
                                                            cur_thresholds, 
                                                            entropy, 
                                                            col_name);
                    if (gain > max_gain)
                    {
                        max_gain = gain;
                        best_threshold = threshold;
                    }

                }
                //Console.WriteLine(best_threshold);
                best_thresholds.Add(best_threshold);
                thresholds.Remove(best_threshold);

            }
            best_thresholds.Sort();
            return best_thresholds;

        }

        /// <summary>
        /// Discretisizes the datatable using given thresholds.
        /// </summary>
        /// <returns>Modified data table</returns>
        /// <param name="dt">Data table</param>
        /// <param name="col_name">The name of the column to discretize</param>
        /// <param name="thresholds">List of thresholds to use for discretization</param>
        public DataTable discretizeDT(DataTable dt, 
                                             string col_name, 
                                             List<double> thresholds)
        {
            DataTable dtCloned = dt.Clone();
            dtCloned.Columns[col_name].DataType = typeof(string);
            foreach (DataRow row in dt.Rows)
            {
                row[col_name] = getDiscrete((double)row[col_name], thresholds);
                dtCloned.ImportRow(row);

            }

            return dtCloned;
        }

        /// <summary>
        /// Prints the dt.
        /// </summary>
        /// <param name="dt">Dt.</param>
        public static void printDT(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                printRowDT(row);
                Console.WriteLine();
            }
        }
        /// <summary>
        /// Prints the dt.
        /// </summary>
        /// <param name="dt">Dt.</param>
        public static void printRowDT(DataRow row)
        {
                foreach (var item in row.ItemArray)
                {
                    Console.Write(item);
                    Console.Write("  ");
                }
        }

        /// <summary>
        /// Gets the best attribute.
        /// </summary>
        /// <returns>The best attribute.</returns>
        /// <param name="dt">Dt - datatable with ONLY discrete parameters.</param>
        /// <param name="attributes">Attributes.</param>
        public string getBestAttribute(DataTable dt, 
                                              List<string> attributes, 
                                              string class_column)
        {
            double entropy = calculateEntropy(dt, class_column);
            double max_gain = 0;
            double cur_gain = 0;
            string best_attribute = attributes[0];
            // for each attribute get the information gain
            foreach (string attribute in attributes)
            {
                cur_gain =  getInformationGainForAttribute(dt, attribute, class_column, entropy);
                if (cur_gain > max_gain)
                {
                    max_gain = cur_gain;
                    best_attribute = attribute;
                }
            }
            return best_attribute;
        }

        public double getInformationGainForAttribute(DataTable dt, 
                                                            string attribute, 
                                                            string class_column, 
                                                            double entropy)
        {

            // get all possible values
            // note: this step is redundant for the iris dataset
            // since in this implementation I'm always taking 4 intervals
            // so we will have 4 distinct values in the column: 1,2,3,4
            var values = dt.AsEnumerable().Select(r => r.Field<string>(attribute)).ToList().Distinct();
            double ent_sum = 0;
            double n_rows = dt.Rows.Count;
            foreach (string val in values)
            {
                DataRow[] selected_rows = dt.Select(attribute + "= '" + val +"'");
                DataTable selected_rows_dt = selected_rows.CopyToDataTable();
                ent_sum += (selected_rows.Count() / n_rows)*calculateEntropy(selected_rows_dt,class_column);

            }
            return entropy - ent_sum;
        }

        /// <summary>
        /// Slits data into test and train with the given threshold.
        /// E.g. if prob = 0.7, then approximately 70% of the dataset dt 
        /// will be in the train dataset
        /// 
        /// </summary>
        /// <returns>The split: a dictionary of two datasets: train and test</returns>
        /// <param name="dt">Dt.</param>
        /// <param name="prob">Prob.</param>
        /// <param name="class_column">Class column.</param>
        public Dictionary<string, DataTable> getSplit(DataTable dt,double prob, string class_column)
        {
            Dictionary<string, DataTable> split = new Dictionary<string, DataTable>();
            int n_rows = dt.Rows.Count;
            DataTable train = dt.Clone();
            DataTable test = dt.Clone();
            // Perform selection in each class to keep the proportion
            var labels = dt.AsEnumerable().Select(r => r.Field<string>(class_column)).ToList().Distinct();
            Random rnd = new Random();
            double cur_prob;
            foreach (string label in labels)
            {
                DataRow[] selected_rows  = dt.Select(class_column + " = '" + label + "'");
                foreach (DataRow row in selected_rows)
                {
                    cur_prob = rnd.NextDouble();
                    if (cur_prob < prob)
                    {
                        train.ImportRow(row);
                    }
                    else {
                        test.ImportRow(row);
                    }
                }
            }
            split.Add("train", train);
            split.Add("test", test);
            return split;

        }

    }
}
