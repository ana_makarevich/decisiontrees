﻿using System;
using System.Collections.Generic;

namespace DecisionTrees
{
    public class Node
    {
        // attribute to shatter by
        public string attribute { get; set; }
        // children nodes
        public List<Node> children { get; set; }
        // class label (if it's a leaf)
        public string label { get; set; }
        // the value of the branch if it's a child
        public string value { get; set; }


        public Node()
        {
            children = new List<Node>();
        }

        public bool isLeaf()
        {
            return (children.Count == 0);
        }

    }

}
